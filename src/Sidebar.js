import React from 'react';
import styled from 'styled-components';



const Nav = styled.div`
height: 100%;
width: 20%;
position: fixed;
z-index: 0;
top: 0;
left: 0;
background-color:  white;
overflow-x: hidden;
padding-top: 10%;

 @media only screen and (max-width: 768px) {
  /* For mobile phones: */
  {
    position: relative;
    padding-top: 5%;
    height: 10%;
    width: 100%;

  }
}

`
;
const NavLink = styled.a`
text-align: center;
text-decoration: none;
font-size: 45px;
padding: 5px;
font-family: 'Caveat', cursive;
display: block;
&:hover {
  color:  #fc721d;
};
@media only screen and (max-width: 768px) {
color: green;
font-size: 30px;
width: 100%;
height:20%;
text-align: center;
font-family: Arial, Helvetica, sans-serif;

}

`;

const NavLinkWrapper = styled.div`
margin-top: 50%;
@media only screen and (max-width: 768px) {
margin-top: 0;
margin-left:auto;
margin-right:auto;
}
`;


export default class Sidebar extends React.Component {
  render(){
    return(
      <Nav>
        <NavLinkWrapper>
          <NavLink>About</NavLink>
          <NavLink>Gallery</NavLink>
          <NavLink>Blogs</NavLink>
          <NavLink>etc</NavLink>
      </NavLinkWrapper>
      </Nav>


    );
  }
}
