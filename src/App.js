import React from 'react';
import Sidebar from './Sidebar';
import Topbar from './Topbar';
import HomeImage from './HomeImageStyled';

const App = () => (
<div>
    <Topbar />
     <Sidebar />
    <HomeImage />

  </div>
  );


export default App;
