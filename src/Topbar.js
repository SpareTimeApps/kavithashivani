import React from 'react';
import styled from 'styled-components';
import logo from './logo.png';

const TopBarContainer = styled.div `
  background:  white;
  padding-top: 10%;
  height: 50%;
  width: 100%;
  @media only screen and (max-width: 768px) {
   /* For mobile phones: */
   {
     margin:auto;
     width: 100%;
     height: 40%;
   }
 }
`;
const AppLogo = styled.img`
display: block;
margin-left: auto;
margin-right: auto;
width: 30%;
@media only screen and (max-width: 768px) {
 /* For mobile phones: */
 {
   display: block;
   margin-left: auto;
   margin-right: auto;
   width: 90%;

 }
}
`;
const TitleTag = styled.a
`font-family: 'Josefin Slab', serif;
text-decoration: none;
font-weight: 300;
text-transform: uppercase;
font-size: 45px;
display: block;
color:#DC143C;

text-align: center;
`;


export default class Topbar extends React.Component {
  render(){
    return(
      <TopBarContainer>
    <TitleTag href="/">Kavitha Shivani Fine art</TitleTag>
    </TopBarContainer>
    );
  }
}
