import React from 'react';
import face from './face.jpg'
import styled from 'styled-components';

const Image = styled.img`
//max-width: 80%;
max-height: 80%;
margin-left: 20%;
margin-top: .1%;
align: center;


max-width: 1170px;
margin: 0 auto;
position: relative;
overflow: hidden;

 @media only screen and (max-width: 768px) {
  /* For mobile phones: */
  {
    width: 100%;
  }
}
`;
export default class HomeImage extends React.Component {
  render(){
    return(
      <Image src={face}>
        </Image>
    );
  }
}
